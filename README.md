# Punctis Engagement
Magento module for Punctis integration.

## Installation via Composer
Declare a `composer.json` file in your Magento project with the following content:

    {
      "name": "bitbull/project",
      "description": "Magento Project",
      "authors": [
        {
          "name": "Bibull",
          "email": "info@bitbull.it"
        }
      ],
      "require": {
        "php": ">=5.5.0",
        "punctis/core": "dev-master",
        "punctis/magento": "dev-master",
        "magento-hackathon/magento-composer-installer": "2.1.1",
        "firegento/psr0autoloader": "dev-master"
      },
      "repositories": [
        {
          "type": "composer",
          "url": "http://packages.firegento.com"
        },
        {
          "type": "vcs",
          "url": "git@github.com:magento-hackathon/Magento-PSR-0-Autoloader.git"
        },
        {
          "type": "vcs",
          "url": "git@bitbucket.org:bitbull/punctis_core.git"
        },
        {
          "type": "vcs",
          "url": "git@bitbucket.org:bitbull/punctis_magento.git"
        }
      ],
      "extra": {
        "magento-root-dir": "./"
      }
    }

Then run the following command in terminal from Magento root dir:

    $ composer install

Specify the following XML node either in your `<magento_root>/app/etc/local.xml` file or in a 
`<magento_root>/app/etc/composer.xml` additional file:

    <config>
        <global>
            <composer_vendor_path><![CDATA[{{root_dir}}/vendor]]></composer_vendor_path>
        </global>
    </config>

## Integration manual
Punctis is a SAAS system which can be used to store customer engagement data outside Magento platform.
Most of its functionality doesn't work "out of the box" but needs custom development; the following is an explanation of 
how this extension will help you integrate Magento with Punctis APIs.

For any API request, in case of error the returned result is an array whose structure is shown below:
    
    Array
    (
        [code] => 500
        [response] => 
        [error] => Array
                    (
                        [code] => Error code
                        [description] => Error message
                    )
    )    
  
### User
User APIs are the ones responsible for registering user actions within the Punctis system.

#### setUser
This method registers an new user to the Engagement Program. This will likely happen whenever a customer accomplishes 
some task within the Magento platform, such as signing up or performing a checkout.
 
To trigger a new user registration within Punctis all you have to do is dispatching a specific event with a specific 
transport object, like shown below:
    
    $arguments = array(
        'username' => 'e-mail address of the customer',
        'card_number' => Magento Customer ID,
        'refcode' => 'Referral code',
    );
    
    $user = array(
        'first_name' => 'Customer first name',
        'last_name' => 'Customer last name',
        'gender' => 'male|female',
        'hometown' => 'Home town',
        'city' => 'City',
        'country' => 'ISO 2 char Country Code',
    );
    
    // To retrieve the ID of legal documents, please refer to Legal Documents section.
    // If no legal documents are needed, initialize an empty array.
    $legalDocuments = array(
        ID of legal document,
        ID of legal document,
        ...
        ID of legal document,
    );
    
    $transport = new Varien_Object(array(
        'arguments' => $arguments,
        'user' => $user,
        'legal_documents' => $legalDocuments,
    ));
    Mage::dispatchEvent('punctis_user_setuser', array('transport' => $transport));    
    
After dispatching the event, the `$transport` object will contain a result you can access as shown below:

    $result = $transport->getResult();
    
In case of success, the returned result is an array whose structure is shown below:

    Array
    (
        [code] => 1
        [response] => Array
            (
                [description] => USER SAVED IN DB
                [user] => Array
                    (
                        [id] => 101591
                        [email] => newuser@mail.com
                        [first_name] => 
                        [last_name] => 
                        [sex] => 
                        [city] => 
                        [hometown] => 
                        [fbid] => 
                        [twaccount] => 
                        [score] => 0
                        [refcode] => 9f323f53
                    )
                [password] => secret
                [bonus] => 
                [email_sent] => 
            )
    )
   
#### getUsersRef
This method should be used to retrieve information about the Member-gets-Members activity of a specific user.

To trigger information for a user all you have to do is dispatching a specific event with a specific transport object
and reading the result, as shown below:

    $identity = array(
        'type' => 'db',
        'key' => 'u.username',
        'value' => 'e-mail address of the customer',
    );
    
    $transport = new Varien_Object(array(
        'identity' => $identity,
    ));
    Mage::dispatchEvent('punctis_user_getusersref', array('transport' => $transport));
    
After dispatching the event, the `$transport` object will contain a result you can access as shown below:

    $result = $transport->getResult();    

In case of success, the returned result is an array whose structure is shown below:
 
    Array
        (
            [code] => 1
            [response] => Array
                (
                    [user] => Array
                        (
                            [id] => 103791
                            [email] => e-mail address of the customer
                            [image] => https://s3-eu-west-1.amazonaws.com/pp.punctis.it/user_placeholder.png
                            [first_name] => 
                            [last_name] => 
                            [sex] => 
                            [city] => 
                            [hometown] => 
                            [fbid] => 
                            [twaccount] => 
                            [score] => 0
                            [refcode] => d770aca2
                            [card_number] => Magento Customer ID
                            [country] => 
                        )
                    [referral_total] => 1
                    [referral_list] => Array
                        (
                            [0] => Array
                                (
                                    [refcode] => 103791
                                    [userid] => 104061
                                    [brandid] => 341
                                    [verified] => 1
                                )
                        )
                )
        ) 

#### getUserInstantRewards
This method should be used to retrieve information about the list of the surprises (or instant rewards) a user gained due to its activity. 

To trigger information for a user all you have to do is dispatching a specific event with a specific transport object
and reading the result, as shown below:

    $identity = array(
        'type' => 'db',
        'key' => 'u.username',
        'value' => 'e-mail address of the customer',
    );
    
    $transport = new Varien_Object(array(
        'identity' => $identity,
    ));
    Mage::dispatchEvent('punctis_user_getuserinstantrewards', array('transport' => $transport));
    
After dispatching the event, the `$transport` object will contain a result you can access as shown below:

    $result = $transport->getResult();    

In case of success, the returned result is an array whose structure is shown below:
 
    Array
    (
        [code] => 1
        [response] => Array
            (
                [rewards] => Array
                    (
                        [0] => Array
                            (
                                [id] => 361
                                [title] => Ambassador Reward
                                [description] => Reward gained by the ambassador on invited user's signup
                                [image] => https://s3-eu-west-1.amazonaws.com/pp.punctis.it/dashboard/zh9GbUtV8LrO/images/surprises/COUPON20.jpg
                                [instructions] => Visit our site, enter your coupon code and gain your reward
                                [couponcode] => AMBASSADOR
                                [validbegin] => 
                                [validend] => 
                            )
    
                        [1] => Array
                            (
                                [id] => 371
                                [title] => Invited User Reward
                                [description] => Reward gained by the invited user at signup
                                [image] => https://s3-eu-west-1.amazonaws.com/pp.punctis.it/dashboard/zh9GbUtV8LrO/images/surprises/COUPON10.jpg
                                [instructions] => Visit our site, enter  your coupon code and get your discount!
                                [couponcode] => INVITEDUSER
                                [validbegin] => 
                                [validend] => 
                            )
                    )
    
                [user] => Array
                    (
                        [id] => 104801
                        [email] => andrea.slomp@gmail.com
                        [image] => https://s3-eu-west-1.amazonaws.com/pp.punctis.it/user_placeholder.png
                        [first_name] => Andrea
                        [last_name] => Slomp
                        [sex] => 
                        [city] => 
                        [hometown] => 
                        [fbid] => 
                        [instagramid] => 
                        [twaccount] => 
                        [score] => 0
                        [refcode] => eace6d3f
                        [card_number] => 
                        [country] => 
                    )
            )
    )


### Point Collection
Point Collection APIs are the ones responsible for retrieving Point Collections details from the Punctis system.

#### getPcActions
This metod is used to retrieve the list of actions included in a given point collection.

To fetch data for a specific point collection all you have to do is dispatching a specific event with a specific transport object
and reading the result, as shown below:

    $arguments = array(
        'pcid' => Mage::helper('arvato_barillacustomization/punctis_data')->getPcid(),
    );
    
    $transport = new Varien_Object(array(
        'arguments' => $arguments,
    ));
    
    Mage::dispatchEvent('punctis_pointcollection_getpcactions', array('transport' => $transport));

After dispatching the event, the `$transport` object will contain a result you can access as shown below:

    $response = $transport->getResult();
    
In case of success, the returned result is an array whose structure is shown below:
    
    Array
        (
            [code] => 1
            [response] => Array
                (
                    [actions] => Array
                        (
                            [0] => Array
                                (
                                    [id] => 1591
                                    [title] => Refer User
                                    [description] => MGM refer a user
                                    [image] => img
                                    [type] => 19
                                    [apicode] => z0RaU4bWNKhXYke2xnO6f1AZp
                                    [rewards] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [times] => 100
                                                    [multiple] => 2
                                                    [ref_points] => 5
                                                    [new_user_points] => 0
                                                    [ref_instantreward] => Array
                                                        (
                                                            [id] => 331
                                                            [title] => test surprise
                                                            [description] => test surprise
                                                            [instructions] => 
                                                            [image] => 
                                                        )
                                                    [new_user_instantreward] => 
                                                )
                                        )
                                    [limits] => 
                                    [translations] => Array
                                        (
                                        )
                                )
                        )
                )
        )
    

## Release notes
The extension uses [semantic versioning v 2.0.0](http://semver.org/) convention.

Note: you can use the **{break}.{feature}.{fix}** formula to easily remember which number has to be changed after some
code changes.

### v 0.8.1 (release tag: v0.8.1-alpha)
* Fix wrong type hint
* Remove wrong reference to $this and add referral parameter

### v 0.8.0 (release tag: v0.8.0-alpha)
* Add helper method to prepare transport object for user registration 

### v 0.7.2 (release tag: v0.7.2-alpha)
* Remove automatic ?debug=true parameter from API endpoint URL

### v 0.7.1 (release tag: v0.7.1-alpha)
* Update documentation
 
### v 0.7.0 (release tag: v0.7.0-alpha)
* Add logic based on `punctis_user_getuserinstantrewards` event
 
### v 0.6.1 (release tag: v0.6.1-alpha)
* Fix module version and dependency from Punctis Core

### v 0.6.0 (release tag: v0.6.0-alpha)
* Add logic based on `punctis_pointcollection_getpcactions` event

### v 0.5.2
* Fix Punctis_Engagement_Model_Logger class declaration

### v 0.5.1
* Fix README.md

### v 0.5.0
* Add debug flag to endpoint url
* Add logic based on `punctis_user_getusersref` event
* Add check for disabled extension in observer methods

### v 0.4.0
* Add result to transport object upon `punctis_user_setuser` event dispatch

### v 0.3.0
* Implement event based logic
* Add logic based on `punctis_user_setuser` event

### v 0.2.0
* Add Logger
* Add some system config entries

### v 0.1.4
* Fix wrong helper class name

### v 0.1.3
* Fix package dependencies

### v 0.1.2
* Fix package names in documentation

### v 0.1.1
* Fix package name

### v 0.1.0
* First commit and module structure
