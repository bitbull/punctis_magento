<?php
class Punctis_Engagement_Model_Observer_PointCollection extends Punctis_Engagement_Model_Observer_Abstract
{
    /** @var  Punctis\Core\Api\v2\PointCollection */
    protected $_api;

    // We have "not injectable" dependencies :(
    function __construct()
    {
        parent::__construct();
        $this->_api = new Punctis\Core\Api\v2\PointCollection($this->_gateway);
    }

    /**
     * @event punctis_pointcollection_getpcactions
     */
    public function getPcActions(Varien_Event_Observer $event)
    {
        if (!$this->_helper->isActive()) {
            return;
        }

        $transport = $event->getTransport();

        $arguments = $transport->getArguments();
        $result = $this->_api->getPcActions($arguments);
        $transport->setResult($result);
    }
}