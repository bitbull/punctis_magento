<?php
abstract class Punctis_Engagement_Model_Observer_Abstract
{
    /**
     * @var Punctis\Core\Gateway
     */
    protected $_gateway;

    /**
     * @var Punctis_Engagement_Helper_Data
     */
    protected $_helper;

    // We have "not injectable" dependencies :(
    function __construct()
    {
        $this->_helper = Mage::helper('punctis_engagement');
        $this->_gateway = new Punctis\Core\Gateway(
            $this->_helper->getApiUrl(),
            $this->_helper->getApiBrandcode(),
            $this->_helper->getApiAuthkey(),
            $this->_helper->getLogger()
        );
    }

}