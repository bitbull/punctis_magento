<?php
class Punctis_Engagement_Model_Observer_User extends Punctis_Engagement_Model_Observer_Abstract
{
    /** @var  Punctis\Core\Api\v2\User */
    protected $_api;

    // We have "not injectable" dependencies :(
    function __construct()
    {
        parent::__construct();
        $this->_api = new Punctis\Core\Api\v2\User($this->_gateway);
    }

    /**
     * @event punctis_user_setuser
     */
    public function setUser(Varien_Event_Observer $event)
    {
        if (!$this->_helper->isActive()) {
            return;
        }

        $transport = $event->getTransport();

        $arguments = $transport->getArguments();
        $user = $transport->getUser();
        $legalDocuments = $transport->getLegalDocuments();

        $result = $this->_api->setUser($arguments, $user, $legalDocuments);
        $transport->setResult($result);
    }

    /**
     * @event punctis_user_getusersref
     */
    public function getUsersRef(Varien_Event_Observer $event)
    {
        if (!$this->_helper->isActive()) {
            return;
        }

        $transport = $event->getTransport();

        $identity = $transport->getIdentity();

        $result = $this->_api->getUsersRef($identity);
        $transport->setResult($result);
    }

    /**
     * @pevent punctis_user_getuserinstantrewards
     */
    public function getuserInstanceRewards(Varien_Event_Observer $event)
    {
        if (!$this->_helper->isActive()) {
            return;
        }

        $transport = $event->getTransport();

        $identity = $transport->getIdentity();

        $result = $this->_api->getUserInstantRewards($identity);
        $transport->setResult($result);
    }
}