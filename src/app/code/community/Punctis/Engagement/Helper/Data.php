<?php
class Punctis_Engagement_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * XML Paths for General configuration constants
     */
    const XML_PATH_PUNCTIS_GENERAL_ACTIVE = 'punctis_engagement/general/active';
    const XML_PATH_PUNCTIS_GENERAL_DEBUG = 'punctis_engagement/general/debug';

    /**
     * XML Paths for API configuration constants
     */
    const XML_PATH_PUNCTIS_API_URL = 'punctis_engagement/api/url';
    const XML_PATH_PUNCTIS_API_BRANDCODE = 'punctis_engagement/api/brandcode';
    const XML_PATH_PUNCTIS_API_AUTHKEY = 'punctis_engagement/api/authkey';

    /** @var  Punctis_Engagement_Model_Logger */
    protected $_logger;

    public function __construct()
    {
        $this->_logger = Mage::getModel('punctis_engagement/logger', $this->isDebug());
    }

    public function isActive()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PUNCTIS_GENERAL_ACTIVE);
    }

    public function getApiUrl()
    {
        $url = Mage::getStoreConfig(self::XML_PATH_PUNCTIS_API_URL);
        return $url;
    }

    public function getApiBrandcode()
    {
        return Mage::getStoreConfig(self::XML_PATH_PUNCTIS_API_BRANDCODE);
    }

    public function getApiAuthkey()
    {
        return Mage::getStoreConfig(self::XML_PATH_PUNCTIS_API_AUTHKEY);
    }

    /**
     * @return Punctis_Engagement_Model_Logger
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    private function isDebug()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PUNCTIS_GENERAL_DEBUG);
    }

    /**
     * @return Varien_Object
     */
    public function getTransportForUserRegistration(Mage_Customer_Model_Customer $customer, $referral = '')
    {
        $gender = $customer->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
            ->getResource()->getAttribute('gender')->getSource()->getOptionText($customer->getGender());

        $arguments = array(
            'username'    => $customer->getEmail(),
            'card_number' => $customer->getId(),
            'refcode'     => $referral,
        );

        $address = $customer->getDefaultBillingAddress();

        $user = array(
            'first_name' => $customer->getFirstname(),
            'last_name'  => $customer->getLastname(),
            'gender'     => $gender ? strtolower($gender) : '',
            'hometown'   => $address ? $address->getCity() : '',
            'city'       => $address ? $address->getCity() : '',
            'country'    => $address ? $address->getCountry() : '',
        );

        $legalDocuments = array();

        $transport = new Varien_Object(array(
            'arguments'       => $arguments,
            'user'            => $user,
            'legal_documents' => $legalDocuments,
        ));
        return $transport;
    }
}
